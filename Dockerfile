FROM docker.io/library/alpine:3.19

RUN apk update
RUN apk add nodejs npm

WORKDIR /data
COPY . .
RUN npm i

EXPOSE 3000
CMD [ "npm", "run", "serve" ]
