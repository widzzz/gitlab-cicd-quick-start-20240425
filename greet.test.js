const greet = require('./greet');

test('print "Hello world"', () => {
  expect(greet()).toBe("Hello world");
});
