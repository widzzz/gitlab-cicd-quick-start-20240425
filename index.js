const express = require('express');
const greet = require('./greet')

const app = express(); 

app.get('/', (req, res) => {
  res.send(greet());
});

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Server is running on port ${port}!`);
});
